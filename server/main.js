var database = new MongoInternals.RemoteCollectionDriver("mongodb://127.0.0.1:27017/meteor");    
var Users = new Mongo.Collection("users_collection", { _driver: database });
var Rate = new Mongo.Collection("rate_collection", { _driver: database });

Meteor.methods({

	login_user : function(login_data)
	{		
		var res_data = Users.find(login_data).fetch();	
		return res_data;
	},

	register_user : function(register_data)
	{
		var res_data = Users.find({email:register_data.email}).fetch();
		if(res_data.length>0)
		{
			return {status:300, message:"User already exists."}
		}
		res_data = Users.insert(register_data);
		register_data._id = res_data
		return {status:200, message:"Your are now loged in.",data:register_data};
	},

	get_list_users : function(user_data)
	{
		var temp_data = Users.aggregate([{$lookup: { from: "rate_collection",localField: "_id",
										foreignField: "user_id", as: "main_docs"}}]);

		var response_data = {main:{},other:[]};

		for(var i in temp_data)
		{
			var temp = temp_data[i].main_docs;			
			if(temp_data[i]._id == user_data.user_id)
			{
				response_data.main._id = temp_data[i]._id;
				response_data.main.first_name = temp_data[i].first_name;
				response_data.main.last_name = temp_data[i].last_name;
				var sumOfRating = 0;
				for(var k in temp)
				{					
					sumOfRating += parseInt(temp[k].rating);
				}
				response_data.main.rating = parseInt(sumOfRating/temp.length);
				response_data.main.ratingArr = [];
				for(var t=1;t<=response_data.main.rating;t++)
				{
					response_data.main.ratingArr.push(t);
				}
			}
			else
			{				
				var tempObj = { _id : temp_data[i]._id,
								first_name : temp_data[i].first_name,
								last_name : temp_data[i].last_name,
								rating : 0, ratingArr : []
							  };

				for(var m in temp)
				{
					if(temp[m].rate_user_id == user_data.user_id)
					{
						tempObj.rating = temp[m].rating;
						for(var key=1;key<=tempObj.rating;key++)
						{
							tempObj.ratingArr.push(key);
						}
						break;
					}					
				}

				response_data.other.push(tempObj);

			}
		}	
		return response_data;
	},

	rate_user : function(rate_data)
	{		
		var res_data = Rate.insert(rate_data);	
		return res_data;
	},

});

