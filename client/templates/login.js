Template.login.events({
	"submit .form-signin": function(event) {
		
		var email = event.target.email.value;
		var password = event.target.password.value;
		var login_data = {email:email,password:password};

		Meteor.call('login_user',login_data,function(err,result)
		{				
				if(err)
				{
					FlashMessages.sendError("Something went wrong");
				}
				else if(result.length<1)
				{
					FlashMessages.sendError("check your credentials");					
				}
				else
				{
					Session.set('userData', result[0]);
					FlashMessages.sendSuccess("You are now logged in");
					Router.go('/dashboard');
				}				
			});

		//Prevent form submission
		return false;
	}
});