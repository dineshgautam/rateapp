
Template.dashboard.onCreated(function () {
	 this.rateUserId = 0;

});

Template.dashboard.events({

	'click .open-insert-modal': function (e, tmpl) {
    e.preventDefault();    
    tmpl.rateUserId  =e.target.id;         
    $('#insertModal').modal({backdrop: "static"});
},

'click #rateUser': function (e,template) {
    e.preventDefault();
    var element = template.find('input:radio[name=star]:checked');

    var rate_data = {user_id:Session.get('userData')._id, rate_user_id : template.rateUserId,
    					 rating:element.value,created_at:new Date()};

    Meteor.call('rate_user',rate_data,function(err,result)
		{				
				if(err)
				{
					FlashMessages.sendError("Something went wrong");					
				}				
				else
				{					
					FlashMessages.sendSuccess("rating success");					
				}				
			});

},



	
});