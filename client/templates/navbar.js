Template.navbar.events({
	"click .logout-btn": function(event) {
		
		FlashMessages.sendSuccess('You are now logged out!');
		Router.go('/');
	}
});